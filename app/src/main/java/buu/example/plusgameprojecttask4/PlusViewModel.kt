package buu.example.plusgameprojecttask4

import android.util.Log
import androidx.lifecycle.ViewModel

class PlusViewModel : ViewModel() {
    init {
        Log.i("PlusViewModel", "PlusViewModel created!")
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("PlusViewModel", "PlusViewModel destroyed!")
    }

}